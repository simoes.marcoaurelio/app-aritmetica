package br.com.apparitmetica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApparitmeticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApparitmeticaApplication.class, args);
	}

}
