package br.com.apparitmetica.services;

import br.com.apparitmetica.DTOs.EntradaDTO;
import br.com.apparitmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO) {
        int numero = 0;
        for(int n: entradaDTO.getNumeros()) {
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO) {
        int numero = 0;
        for(int n: entradaDTO.getNumeros()) {
            if (numero == 0) {
                numero += n;
            } else {
                numero -= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO) {
        int numero = 1;
        for(int n: entradaDTO.getNumeros()) {
            numero *= n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        int numero = 1;
        for(int n: entradaDTO.getNumeros()) {
            numero = n / numero;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

}
