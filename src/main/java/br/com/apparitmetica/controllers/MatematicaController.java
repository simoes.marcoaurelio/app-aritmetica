package br.com.apparitmetica.controllers;

import br.com.apparitmetica.DTOs.EntradaDTO;
import br.com.apparitmetica.DTOs.RespostaDTO;
import br.com.apparitmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {

        validaEntrada(entradaDTO);

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {

        validaEntrada(entradaDTO);

        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {

        validaEntrada(entradaDTO);

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {

        validaEntradaDivisao(entradaDTO);

        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }

    private void validaEntrada(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos dois números para serem calculados");
        }

        for (int n : entradaDTO.getNumeros()) {
            if (n <= 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é permitido número zero ou menor que zero");
            }
        }
    }

    private void validaEntradaDivisao(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos dois números para serem calculados");
        }

        int numeroAnterior = 0;

        for (int n : entradaDTO.getNumeros()) {
            if (n <= 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é permitido número zero ou menor que zero");
            }

            if (n < numeroAnterior) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O número anterior não pode ser maior que o número seguinte");
            } else {
                numeroAnterior = n;
            }

        }
    }
}